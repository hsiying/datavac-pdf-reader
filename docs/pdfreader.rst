pdfreader package
=================

Submodules
----------

pdfreader.data\_gen module
--------------------------

.. automodule:: pdfreader.data_gen
   :members:
   :undoc-members:
   :show-inheritance:

pdfreader.dictionary module
---------------------------

.. automodule:: pdfreader.dictionary
   :members:
   :undoc-members:
   :show-inheritance:

pdfreader.lab\_extractor module
-------------------------------

.. automodule:: pdfreader.lab_extractor
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pdfreader
   :members:
   :undoc-members:
   :show-inheritance:
