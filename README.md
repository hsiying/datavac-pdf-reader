# MRR-PDF-reader
Medical Record Reader: for reading PDF lab reports and exporting results into MRR App.
**Note** that this package aims solely to extract text from PDF documents. It is **not responsible**
for *post-editing* or *post-processing*. If the original file included errors, this program will **read 'as it is'**.


Here is the [documentation](https://hsiying.gitlab.io/datavac-pdf-reader/#) of this library.
### Authors
V.Truong, S. Huang
## Getting started

## Installation

### Requirements

In order to install `pdftotext`, you need have `poppler` installed in your system.

`pip install -e /path/to/repo/`
if you are inside the repo and already activated the virtual environment, use: 
`pip install -e .`

## Usage

```
from pdfreader.lab_extractor import LabsheetPdf
from pdfreader.mibi_reader import ScanMIBI

ScanMIBI(file_path)()

labpdf = LabsheetPdf(file_path)
labpdf.get_data()

```
# Contribution


If you are reporting a bug, please include:

* Your operating system name and version.
* Any details about your local setup that might be helpful in troubleshooting.
* Detailed steps to reproduce the bug.

### Fix Bugs

Look through the GitLab issues for bugs. Anything tagged with "bug" and "help
wanted" is open to whoever wants to implement it.

### Implement Features

At the development stage, we have migrate the planned features on Jira. However, if you think any novel features need to be implemented, you can open the issue here.
