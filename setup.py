"""Setup file for the pacakge. """
import os
import re
import subprocess
import codecs
from setuptools import setup, find_packages
# from distutils.core import setup


here = os.path.abspath(os.path.dirname(__file__))


def read(*parts):
    """
    Read file

    Returns
    -------
    file
    """
    with codecs.open(os.path.join(here, *parts), 'r') as file_page:
        return file_page.read()


def find_version(*file_paths):
    """
    Find the version from the init.py file

    Returns
    -------
    str
        version

    Raises
    ------
    RuntimeError
        If not found.
    """
    version_file = read(*file_paths)
    version_match = re.search(r"^__version__ = ['\"]([^'\"]*)['\"]",
                              version_file, re.M)
    if version_match:
        return version_match.group(1)
    raise RuntimeError("Unable to find version string.")


def poppler_cpp_at_least(version):
    """
    Check if the environment contains popler
    """
    try:
        subprocess.check_call(
            ["pkg-config", "--exists", "poppler-cpp >= {}".format(version)]
        )
    except subprocess.CalledProcessError:
        return False
    except OSError:
        print("WARNING: pkg-config not found--guessing at poppler version.")
        print("         If the build fails, install pkg-config and try again.")
    return True


with open("README.md", "r") as fh:
    long_description = fh.read()

setup(name='mrr-pdfreader',
      version=find_version('pdfreader', '__init__.py'),
      license='MIT',
      description='Reading lab test from a PDF record.',
      long_description=long_description,
      long_description_content_type="text/markdown",
      url="https://hsiying.gitlab.io/datavac-pdf-reader/#",
      author='S. Huang, V. Truong',
      author_email='data.vac.notificationl@gmail.com',
      packages=find_packages(),
      python_requires='>=3.7',
      install_requires=[
          'datetime',
          'numpy',
          'typing_extensions',
          'typing',
          'python-dateutil',
          'pdftotext>=2.1.4'
      ])
