"""Test pdfreader.utils module"""
import pytest
from pdfreader.utils import (get_flexible_dt, format_datetime,
                             get_header, extract_info, REGEX_TIME)


@pytest.fixture(scope='session', name='header')
def header_text():
    """
    Setup testing text

    Returns
    -------
    list
        Contains date of birth, analysis datetime, case number and
         admission datetime.
    """
    return ['Születési dátum...:  1959.01.09',
            'Vizsgálat idõpontja: 2017.04.07 07:34',
            'Esetszám: 12345678',
            'Date of admission: 2020.09.13'
            ]


def test_get_header(header):
    """
    Test lab_extractor.get_header
    """
    expect = {'casenumber': None,
              'birthdate': '1959.01.09',
              'admission_datetime': None,
              'analysis_datetime': None}
    actual = get_header(header[0], None, None, None, None)
    assert expect == actual, f'expected {expect}, got {actual}.'


def test_extract_info(header):
    """
    Test lab_extractor.extract_info
    """
    expect = '2017.04.07 07:34'
    actual = extract_info(header[1], 'Vizsgálat idõpontja', REGEX_TIME)
    assert expect == actual, f'expected {expect}, got {actual}.'


def test_get_flexible_dt():
    """
    Test utils.flexible_dt
    """
    example = '2017.04.08   '
    expect = '2017.04.08'
    actual = get_flexible_dt(example)
    assert expect == actual, f'expected {expect}, got {actual}.'
    example = '2017.04.08 11:223  '
    expect = '2017.04.08 11:22'
    actual = get_flexible_dt(example)
    assert expect == actual, f'expected {expect}, got {actual}.'


def test_format_datetime():
    """
    Test utils.format_datetime
    """
    example = '2017.04.08   '
    expect = '2017.04.08 00:00'
    actual = format_datetime(example)
    assert expect == actual, f'expected {expect}, got {actual}.'
