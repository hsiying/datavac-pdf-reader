""" Unit Test module mibi_reader"""
import pytest
from pdfreader.mibi_reader import (get_bacteria_header,
                                   get_drug_resistance,
                                   #    get_flexible_dt,
                                   get_mintat,
                                   post_process_resist)


@pytest.fixture(name='line', scope='module')
def line_text():
    """
    Testing line text.
    """
    return ['MINTAT:hemokultura aerob,',
            '    Corynebacterium striatum',
            'Vancomycin:Emic:2?g/ml, Teicoplanin:Emic:1?g/ml, \
            Ciprofloxacin:R,',
            'Hk-palack 6,06 napra lett pozitív.; \
                Aerob baktérium nem tenyészett ki.;',
            'Gomba tenyésztés negativ; Anaerob baktérium nem tenyészett ki;',
            '  Levofloxacin:R, Sulfamethoxasole/trimethoprim:E, \
            Doxycyclin:E, Amikacin:E  '
            ]


def test_get_bacteria_header(line):
    """
    Test mibi_reader.get_bacteria_header
    """
    expect = ['microorganism', 'Corynebacterium striatum']
    actual = get_bacteria_header(line[1])
    assert expect == actual, f'expected {expect}, got {actual}.'
    expect = ['microorganism', 'Aerob baktérium negativ']
    actual = get_bacteria_header(line[3])
    assert expect == actual, f'expected {expect}, got {actual}.'
    expect = [['microorganism', 'Anaerob baktérium negativ'],
              ['microorganism', 'Gomba negativ']]
    actual = get_bacteria_header(line[4])
    assert expect == actual, f'expected {expect}, got {actual}.'


def test_get_drug_resistance(line):
    """
    Test mibi_reader.get_drug_resistance
    """
    expect = [['Vancomycin', 'Emic', '2?g/ml'],
              ['Teicoplanin', 'Emic', '1?g/ml'],
              ['Ciprofloxacin', 'R']]
    actual = get_drug_resistance(line[2])
    assert expect == actual, f'expected {expect}, got {actual}.'
    expect = [['Levofloxacin', 'R'],
              ['Sulfamethoxasole/trimethoprim', 'E'],
              ['Doxycyclin', 'E'],
              ['Amikacin', 'E']]
    actual = get_drug_resistance(line[5])
    assert expect == actual, f'expected {expect}, got {actual}.'


def test_get_mintat(line):
    """
    Test mibi_reader.get_mintat
    """
    expect = 'hemokultura aerob'
    actual = get_mintat(line[0])
    assert expect == actual, f'expected {expect}, got {actual}.'


def test_post_process_resist():
    """
    Test mibi_reader.post_process_resist
    """
    example = ['Vancomycin', 'Emic', '2?g/ml']
    expect = ['Vancomycin', 'Emic 2?g/ml']
    actual = post_process_resist(example)
    assert expect == actual, f'expected {expect}, got {actual}.'
    example = ['!=MRSA törzs!', '!']
    expect = ['MRSA', 'MRSA törzs/True']
    actual = post_process_resist(example)
    assert expect == actual, f'expected {expect}, got {actual}.'
