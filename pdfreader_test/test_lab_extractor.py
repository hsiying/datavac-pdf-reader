""" Test module lab_extractor """
# from os.path import expanduser
import pytest
from pdfreader.lab_extractor import\
    (search_analysis_datetime, get_result1, get_result2,
     append_result_line, skip_footer, get_report_type,
     get_age, verify_values)

REGEX_TIME = r"[0-9]{4}\.[0-9]{2}\.[0-9]{2} [0-9]{2}:[0-9]{2}"


@pytest.fixture(scope='session', name='cols')
def cols_output():
    """
    Simulate cols output
    """
    return [['LDH',  'U/L',  '<248', 'F'],
            ['Vizelet Nitrit', 'Negatív', 'F'],
            ['Vizelet üledék baktérium', '++++', 'F'],
            ['Vizelet üledék fvs conglom.', 'zsúfolva', 'F'],
            ['Urobilinogen', 'normális', 'F']
            ]


def test_verify_values(cols):
    """
    Test lab_extractor.verify_valueexpect = var1
    actual = var2
    assert expect == actual, f'expected {expect}, got {actual}.'s
    """
    expect = ['LDH', '-999', 'U/L', '<248', 'F']
    actual = verify_values(cols[0])
    assert expect == actual, f'expected {expect}, got {actual}.'

    expect = ['Vizelet Nitrit', 'Negatív', 'F']
    actual = verify_values(cols[1])
    assert expect == actual, f'expected {expect}, got {actual}.'

    expect = ['Vizelet üledék baktérium', '++++', 'F']
    actual = verify_values(cols[2])
    assert expect == actual, f'expected {expect}, got {actual}.'

    expect = ['Vizelet üledék fvs conglom.', 'zsúfolva', 'F']
    actual = verify_values(cols[3])
    assert expect == actual, f'expected {expect}, got {actual}.'

    expect = ['Urobilinogen', 'normális', 'F']
    actual = verify_values(cols[4])
    assert expect == actual, f'expected {expect}, got {actual}.'


def test_search_analysis_datetime():
    """
    Test lab_extractor.search_analysis_datetime
    """
    example = 'Vizsgálat idõpontja: 2017.04.07 07:34'
    expect = '2017.04.07'
    actual = search_analysis_datetime(
        r"[0-9]{4}\.[0-9]{2}\.[0-9]{2}", example, None)
    example2 = '2010/09/01'
    assert expect == actual, f'expected {expect}, got {actual}.'
    expect = None
    actual = search_analysis_datetime(
        r"[0-9]{4}\.[0-9]{2}\.[0-9]{2}", example2, None)
    assert expect == actual, f'expected {expect}, got {actual}.'


def test_get_result1():
    """
    Test lab_extractor.get_result1
    """
    example = 'ctO2    18.0        vol%'
    expect = ['ctO2', '18.0', 'vol%']
    actual = get_result1(example)
    assert expect == actual, f'expected {expect}, got {actual}.'


def test_get_result2():
    """
    Test lab_extractor.get_result2
    """
    example = 'CRP      116.50       mg/L    H    <5.00      F'
    expect = ['CRP', '116.50', 'mg/L', '<5.00', 'F']
    actual = get_result2(example, 5)
    assert expect == actual, f'expected {expect}, got {actual}.'


def test_append_result_line():
    """
    Test lab_extractor.append_result_line
    """
    example = ['Vizelet üledék fehérvérsejt', '1926/HPF', 'F']
    example2 = ['Validálta:', 'Superdoctor dr.']
    expect1 = True
    actual1 = append_result_line(example)
    assert expect1 == actual1, f'expected {expect1}, got {actual1}.'
    expect = False
    actual = append_result_line(example2)
    assert expect == actual, f'expected {expect}, got {actual}.'


def test_skip_footer():
    """
    Test lab_extractor.skip_footer
    """
    example1 = 'Muster M. - Telj. AZ.: 12345678 \
        Nyomtatva: abc19 2019.06.11 19:35'
    example2 = 'e-MedSolution    Oldal    1'
    expect1 = True
    actual1 = skip_footer(example1)
    assert expect1 == actual1, f'expected {expect1}, got {actual1}.'
    expect = True
    actual = skip_footer(example2)
    assert expect == actual, f'expected {expect}, got {actual}.'


def test_get_report_type():
    """
    Test lab_extractor.get_report_type
    """
    example = 'Érték       Egység     Referencia tart.'
    expect = (2, 5)
    actual = get_report_type(example, 0, 0)
    assert expect == actual, f'expected {expect}, got {actual}.'


def test_get_age():
    """
    Test lab_extractor.get_age
    """
    birthdate = '1959.01.09'
    admissiondate = None
    labdate = '2017.04.07 07:34'
    expect1 = 58
    actual1 = get_age(birthdate, analysis_datetime=labdate)
    assert expect1 == actual1, f'expected {expect1}, got {actual1}.'
    expect = None
    actual = get_age(birthdate, admission_date=admissiondate)
    assert expect == actual, f'expected {expect}, got {actual}.'
