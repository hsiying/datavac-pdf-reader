"""Read PDF from Bacterial culture report"""
import re
from typing import List
import pdftotext
import numpy as np
from pdfreader.utils import get_flexible_dt, format_datetime, get_header

MICROORG_DICT = ['Corynebacterium striatum',
                 'Staphylococcus aureus',
                 'Streptococcus pyogenes',
                 'Escherichia coli',
                 'Staphylococcus hominis',
                 'Aerob baktérium',
                 'Anaerob baktérium',
                 'Gomba']


DRUG_RESIST = ['Amikacin', 'Amoxicillin',
               r'Amoxicillin\s?/\s?clav.sav',
               'Ampicillin', r'Ampicillin\s?/\s?sulbactam', 'Azithromycin',
               'Cefalexin', 'Cefazolin', 'Cefepime',
               'Cefixim', 'Cefotaxim', 'Ceftazidim',
               'Ceftriaxon', 'Cefuroxim', 'Ciprofloxacin',
               'Clarithromycin', 'Clindamycin', 'Doxycyclin',
               'Ertapenem', 'Erythromycin', 'Fosfomycin', 'Gentamicin',
               'Imipenem', 'Levofloxacin', 'Linezolid', 'Meropenem',
               'Moxifloxacin', 'Nitrofurantoin',
               'Norfloxacin', 'Ofloxacin', 'Oxacillin',
               'Penicillin', r'Piperacillin\s?/\s?Tazobactam',
               'Rifampicin', 'Streptomycin',
               r'Sulfamethoxasole\s?/\s?trimethoprim', 'Teicoplanin',
               'Tigecyclin', 'Tobramycin', 'Vancomycin', 'MRSA?']

REGEX_NEG = [r'N?n?em', r'N?n?egativ']
DRUG_RESIST_COMPLIED = [re.compile(i) for i in DRUG_RESIST]


def search_negative(line: str) -> bool:
    """
    Search for negation words in the line.

    Parameters
    ----------
    line : str
        Text line.

    Returns
    -------
    bool
        True if found; False otherwise.
    """
    return any(True for i in REGEX_NEG if re.findall(i, line))


def get_bacteria_header(line: str, bact: list = None) -> list:
    """
    Check if text contains the header of bacteria, if yes return a dict.

    Parameters
    ----------
    line : str
        Text line
    bact : list, optional
        bacteria name, by default None

    Returns
    -------
    list
        ['microorganism','bacteria name'] or bact
    """
    bac_header = [re.findall(i, line)
                  for i in MICROORG_DICT if re.findall(i, line)]

    if len(bac_header) == 1:
        # check negative
        if search_negative(line):
            # attach microorganism title per Hai
            bac = ['microorganism', bac_header[0][0] + ' negativ']
        else:
            bac = ['microorganism', bac_header[0][0]]
    if len(bac_header) < 1:
        bac = bact
    if len(bac_header) > 1:
        if search_negative(line):
            # add microorganisim title
            bac = [['microorganism', str(i[0]) + ' negativ']
                   for i in bac_header]
    return bac


def get_drug_resistance(line: str) -> list:
    """
    Extract drug resistance result from the line.

    Parameters
    ----------
    line : str
        text may or may not contain drug resistance test

    Returns
    -------
    list
        ['tested drug', 'test result']
    """
    try:
        test_items = line.strip().split(',')
        drugs = [item.strip().split(':') for item in test_items if item]
        return [drug for drug in drugs
                if any(rex.match(drug[0]) for rex in DRUG_RESIST_COMPLIED)]
    except:  # pylint: disable=W0702 # noqa: E722
        return None


def get_mintat(line: str) -> str:
    """
    Extract MINTAT result

    Parameters
    ----------
    line : str
        Text line may or maynot contain MINTAT result

    Returns
    -------
    str
        MINTAT result if contains.
    """

    return line.split(':')[1].replace(',', '') if 'MINTAT' in line else None


# def get_flexible_dt(line: str) -> str:
#     """
#     Extract datetime with or without HH:MM.

#     Parameters
#     ----------
#     line : str
#         Text line

#     Returns
#     -------
#     str
#         Datetime or date YYYY.MM.DD or YYYY.MM.DD HH:MM
#     """
#     if isinstance(line, list):
#         line = line[0]
#     regex_dt = r"([0-9]{4}\.[0-9]{2}\.[0-9]{2})\s?([0-9]{2}:[0-9]{2})?"
#     found = re.findall(regex_dt, line)
#     return ' '.join(found[0]).strip() if found else None


def post_process_resist(resist: list) -> List[str]:
    """
    Format the extracted info list.

    Parameters
    ----------
    resist : list
        List of extracted info, two to three elements.

    Returns
    -------
    list
        Formated list.
    """
    if len(resist) == 3:
        resist = [resist[0], ' '.join(resist[-2:])]
    if any('MRSA törzs' in i for i in resist):
        resist = ['MRSA', 'MRSA törzs/True']
    return resist


class ScanMIBI:
    """
    Extract the information from MiBi file.
    """

    def __init__(self, filename) -> None:
        self.pdf = pdftotext.PDF(  # pylint: disable=c-extension-no-member
            open(filename, "rb"))
        self.mibi = None

    def get_all(self) -> List[str]:
        """Print text from the lab sheet"""
        output = '\n'.join(self.pdf)
        return output

    def _get_results(self):
        """ Extract culture results."""
        header = {'casenumber': None,
                  'birthdate': None,
                  'admission_datetime': None,
                  'analysis_datetime': None}
        micro_orgs = {}
        resistance = []
        bact = []
        sheet_id = {'sheet_id': 3}
        mintat = {}
        previous_bact = None

        pages = self.get_all()
        for line in pages.splitlines():
            header = get_header(line,
                                header['casenumber'],
                                header['birthdate'],
                                header['admission_datetime'],
                                header['analysis_datetime'])
            # look   for bacteria name                             #
            bact = get_bacteria_header(line, bact)
            # search for MINTAT result
            mintat_ = get_mintat(line)
            if mintat_:
                mintat.update({'MINTAT': mintat_})
            # search for analysis date
            analysis_dt = get_flexible_dt(line)
            # gurantee the date is after MINTAT appearance
            # this is the correct location to find the date.
            if analysis_dt and mintat:
                header['analysis_datetime'] = format_datetime(analysis_dt)
                continue
            # reach to the end of doc, stop searching
            if '.'*32 in line:
                break
            # if found bacteria name and not reach to the MINTAT line
            if bact and not mintat_:
                # if found a new bacteria name reset the resistance list
                if previous_bact != bact[1]:
                    resistance = []
                    previous_bact = bact[1]
                    # if two bact names in one line
                    if len(np.array(bact).shape) > 1:
                        resistance.extend(bact)
                        micro_orgs.update({i[1]: i for i in bact})
                    else:
                        # one bact name in one line
                        resistance.append(bact)
                        micro_orgs.update({bact[1]: resistance})
                    continue
                # search drug resistance test line
                resist = get_drug_resistance(line)
                # if found the line
                if resist:
                    # squeeze lists to two element lists
                    resist_post = [post_process_resist(i) for i in resist]
                    resistance.extend(resist_post)
                    micro_orgs.update({bact[1]: resistance})

        self.mibi = {**header, **sheet_id, **
                     mintat, **{'lab_results': micro_orgs}}
        return self

    def _postprocess(self):
        """
        Format lab result to input of frontend.
        """
        lab = self.mibi['lab_results']
        i = 1
        lab_results = []
        for _, val in lab.items():
            col_name = 'microorganism' + str(i)
            lab_results.append([col_name, val])
            i += 1
        self.mibi.update({'lab_results': lab_results})
        return self

    def __call__(self) -> dict:
        """
        __Call__ function to return to formated extraction results.

        Returns
        -------
        dict
            Store results in a dictionary to be used in the frontend.
        """
        self._get_results()
        self._postprocess()
        return self.mibi
