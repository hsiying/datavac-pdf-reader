""" Module to read and extract data from labsheet."""

# from __future__ import print_function
import logging
import re
import datetime
from typing import Tuple, List
from dateutil.relativedelta import relativedelta
import pdftotext
from pdfreader.utils import get_header


REGEX_TIME = r"[0-9]{4}\.[0-9]{2}\.[0-9]{2} [0-9]{2}:[0-9]{2}"
REGEX_DATE = r"[0-9]{4}\.[0-9]{2}\.[0-9]{2}"
REGEX_STRING = r"[0-9a-zA-Z]+"

BIRTHDATE_LIST = ["Születési dátum", "Date of birth", "Date of birth "]
CASENUMBER_LIST = ["Casenumber", "Esetszám"]
ADMISSIONDATE_LIST = ["Date of admission", "Felvételi dátum"]
ANALYZEDATE_LIST = ["Vizsgálat idõpontja"]

INVISTAGATION_LIST = ["Investigations", "Vizsgálatok"]
RESULT_LIST = ["Result:", "Eredmény:"]
PRINTED_LIST = ["Printed", "Nyomtatva"]
PAGE_LIST = ["Page", "Oldal"]

# lines to ignore
CODE_LIST = ["Code", "Kód"]
ENTRY_LIST = ["Entry", "Megnevezés"]
VALUE_LIST = ["Value", "Érték"]
PASS_LIST = ["Validálta:"]

HEADER_INVESTIGATION = ["Value", "Unit", "Reference",
                        "Érték", "Egység", "Referencia tart."]

NONEDETECT = ['haemolizis miatt nem ertekelheto',
              'cannot be evaluated due to hemolysis']

REGEX_VALID_RESULTS = [r'N?n?egatíve?', r'Poz?s?iti?í?ve?', r'U?u?nknown',
                       r'Ismeretlen', r'N?n?orma?á?li?s?', r'\++', r'\d',
                       r'zsúfolva', '-', 'A', 'B', 'AB', 'O']


def verify_values(cols: List[str]) -> List[str]:
    """
    Verify if the extracted result col contains valid values.
    If no valid value found, insert the value of -999.

    Parameters
    ----------
    cols : List[str]
        list of strings of which the second position contains
        the result values.

    Returns
    -------
    List[str]
        Updated list.
    """
    contain_value = any(True for regex in REGEX_VALID_RESULTS
                        if re.findall(regex, cols[1]))
    if not contain_value:
        cols.insert(1, '-999')
    return cols


def search_analysis_datetime(regex_date: str,
                             line: str,
                             analysis_datetime: str) -> str:
    """
    Search and extract analysis datetime in the line.

    Parameters
    ----------
    REGEX_DATE : str
        Regular expression pattern
    line : str
        text lines
    analysis_datetime : str
        analysis datetime from previous extraction

    Returns
    -------
    str
        Date time of analysis
    """
    line_search = re.search(regex_date, line)
    if analysis_datetime is None and line_search:
        try:
            datetime_ = datetime.datetime.strptime(
                line_search.group(0),
                '%Y.%m.%d')
            analysis_datetime = datetime_.strftime('%Y.%m.%d')
        except ValueError:
            analysis_datetime = None
    return analysis_datetime


def get_result1(line: str, **ncol) -> dict:  # pylint: disable=unused-argument
    """
    Extract lab results

    Parameters
    ----------
    line : str
        Text to be parsed

    Returns
    -------
    dict
        Extracted lab results
    """
    cols = [x.strip()
            for x in line.split("   ") if x.strip() != ""]
    return cols


def get_result2(line: str,
                ncol: int) -> dict:
    """
    Extract lab report results.

    Parameters
    ----------
    line : str
        Text line
    ncol : int
        Number of columns

    Returns
    -------
    dict
        Lab report results
    """

    cols = [x.strip() for x in line.split("   ") if x.strip() != ""]

    if len(cols) > 1 and cols[1].strip() in ["#", "%", "(%)"]:
        cols[0] = cols[0].strip() + " " + cols[1].strip()
        del cols[1]

    if len(cols) == ncol + 1:
        cols = cols[0:3] + cols[4:len(cols)]
    return cols


def append_result_line(cols: str, next_result: bool = True) -> bool:
    """
    Return if current line is result.
    If true, append the current line in the lab result.

    Parameters
    ----------
    cols : str
        Infor extracted from the previous operation
    next_result : bool
        Previous setup for next_result, by default True

    Returns
    -------
    bool
        True, append the current line to the result
        False, skip the current line
    """
    if cols == "":
        return False

    if len(cols) > 1:
        if cols[0] in CODE_LIST and cols[1] in ENTRY_LIST:
            next_result = False

        if cols[0].strip() in VALUE_LIST:
            # continue
            next_result = False

        if cols[0].strip() == "" or cols[0] in PASS_LIST:
            next_result = False
    return next_result


def skip_footer(line: str) -> bool:
    """
    Detect is current line is in footer to skip.

    Parameters
    ----------
    line : str
        text lines

    Returns
    -------
    bool
        True, skip current line
        False, current line is not footer
    """
    return sum(1 for word in PRINTED_LIST +
               CASENUMBER_LIST + PAGE_LIST if word in line) > 0


def get_report_type(line: str,
                    sheet_id: int = 0,
                    ncol: int = 0) -> Tuple[int, int]:
    """
    Detect report type from the line.

    Parameters
    ----------
    line : str
        Text lines

    Returns
    -------
    Tuple[int, int]
        First return sheet_id
        Second return ncol
    """
    if sheet_id and ncol:
        return sheet_id, ncol
    if line in RESULT_LIST:
        return 1, 3
        # if "Value" in line and "Unit" in line and "Reference" in line:
    if sum([word in line for word in HEADER_INVESTIGATION]) == 3:
        return 2, 5
    return 0, 0


def get_age(birthdate: str,
            **dt_args: str) ->\
        dict:  # pylint: disable=inconsistent-return-statements
    """
    Calculate age from medical record.

    Parameters
    ----------
    birthdate : str
        Birth date info extracted from report
    **dt_args: str
        Datetime info extracted from report, can be:
        admission_datetime or analysis_datetime.

    Returns
    -------
    dict
        Age in years.

    Raises
    ------
    KeyError
        If dt_args not provided in one of the two options:
        [admission_datetime, analysis_datetime]
    """
    dt_formats = {'admission_datetime': '%Y.%m.%d %H:%M',
                  'analysis_datetime': '%Y.%m.%d %H:%M'}
    var = []
    try:
        var.append(dt_args.get('admission_datetime', ''))
        var.append(dt_args.get('analysis_datetime', ''))
        dt_vars = [i for i in var if i]
        if dt_vars:
            dt_var = dt_vars[0]
        else:
            raise ValueError('No available datetime record.')
        dt_format = dt_formats.get(list(dt_args.keys())[0])
    except ValueError:
        logging.warning(f"Incorrect argument key: {dt_args}"
                        "options are: admission_datetime, analysis_datetime"
                        "or both of these variables are None type.")
        return None

    birthdatetime = datetime.datetime.strptime(birthdate, '%Y.%m.%d')
    ref_datetime = datetime.datetime.strptime(dt_var, dt_format)
    if ref_datetime:
        time_diff = relativedelta(ref_datetime, birthdatetime)
        return time_diff.years


class LabsheetPdf:
    """
    A class to extract data from lab sheet pdf file.

    Attributes
    ----------
        filename (str), name of the pdf file.
        pdf (list), list of strings extracted from pdf.

    Returns
    -------
    dict
        Extracted information from PDF file.
    """

    def __init__(self, filename):
        self.filename = filename
        self.pdf = pdftotext.PDF(  # pylint: disable=c-extension-no-member
            open(filename, "rb"))

    def get_all(self) -> List[str]:
        """Print text from the lab sheet"""
        output = '\n'.join(self.pdf)
        return output

    def get_data(self):
        """Return a dictionnary with data information"""
        lab_result = []
        header = {'casenumber': None,
                  'birthdate': None,
                  'admission_datetime': None,
                  'analysis_datetime': None}
        results_functions = {1: get_result1,
                             2: get_result2}
        sheet_id, ncol = 0, 0

        # merge pages together
        pages = self.get_all()

        for line in pages.splitlines():
            # parse header to get patient info
            header = get_header(line,
                                header['casenumber'],
                                header['birthdate'],
                                header['admission_datetime'],
                                header['analysis_datetime'])

            # skip lines in footnotes
            if skip_footer(line):
                continue

            # start extract lab results
            # get report type: sheet_id, ncol
            sheet_id, ncol = get_report_type(line, sheet_id, ncol)
            # extract results in the line by report type
            if sheet_id > 0:
                # start to extract results
                get_result = results_functions.get(sheet_id, TypeError)
                cols = get_result(line, ncol=ncol)
                if len(cols) <= 1:
                    search_analysis_datetime(
                        REGEX_DATE, line, header['analysis_datetime'])
                    continue
                if len(cols) == 2:
                    cols.append("")
                if append_result_line(cols):
                    cols = verify_values(cols)
                    lab_result.append(cols)
            else:
                continue

        # end - reading pdf
        lab_results = {'lab_results': lab_result}

        # calculate age
        birthdate = header['birthdate']
        admission_datetime = header['admission_datetime']
        analysis_datetime = header['analysis_datetime']
        age = {'age': get_age(birthdate, analysis_datetime=analysis_datetime)}
        if not list(age.values())[0]:
            logging.warning(
                "Coudn't find available datetime to calculate age.")
            age = {'age': get_age(
                birthdate, admission_datetime=admission_datetime)}

        return {**header, **{'sheet_id': sheet_id}, **lab_results, **age}
