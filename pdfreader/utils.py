""" Utility functions shared modules"""
import re
import datetime
from typing import Any, List

BIRTHDATE_LIST = ["Születési dátum", "Date of birth", "Date of birth "]
CASENUMBER_LIST = ["Casenumber", "Esetszám"]
ADMISSIONDATE_LIST = ["Date of admission", "Felvételi dátum"]
ANALYZEDATE_LIST = ["Vizsgálat idõpontja"]

REGEX_TIME = r"[0-9]{4}\.[0-9]{2}\.[0-9]{2} [0-9]{2}:[0-9]{2}"
REGEX_DATE = r"[0-9]{4}\.[0-9]{2}\.[0-9]{2}"
REGEX_STRING = r"[0-9a-zA-Z]+"


def get_flexible_dt(line: str) -> str:
    """
    Extract datetime with or without HH:MM.

    Parameters
    ----------
    line : str
        Text line

    Returns
    -------
    str
        Datetime or date YYYY.MM.DD or YYYY.MM.DD HH:MM
    """
    if isinstance(line, list):
        line = line[0]
    regex_dt = r"([0-9]{4}\.[0-9]{2}\.[0-9]{2})\s?([0-9]{2}:[0-9]{2})?"
    found = re.findall(regex_dt, line)
    return ' '.join(found[0]).strip() if found else None


def format_datetime(dt_str: str) -> str:
    """
    Format datetime into "%Y.%m.%d %H:%M"

    Args:
        dt_str (str): datetime string

    Returns:
        str: formatted datetime string
    """
    if len(dt_str.strip()) < 16:
        time_elemt = [int(i) for i in dt_str.split('.')]
        d_t = datetime.datetime(*time_elemt)
        return d_t.strftime("%Y.%m.%d %H:%M")
    return dt_str


def get_header(line: str,
               casenumber: str,
               birthdate: str,
               admission_datetime: str,
               analysis_datetime: str) -> dict:
    """
    Extract header information.

    Parameters
    ----------
    line : str
        Line text to be parsed
    casenumber : str
        Numeric case ID
    birthdate : str
        Birth date
    admission_datetime : str
        Admission date
    analysis_datetime : str
        Lab analysis date

    Returns
    -------
    dict
        Extracted subject's info
    """
    line = line.strip()
    if not casenumber:
        for word in CASENUMBER_LIST:
            case = extract_info(line, word, REGEX_STRING)
            if case:
                casenumber = case

    if not birthdate:
        for word in BIRTHDATE_LIST:
            bday = extract_info(line, word, REGEX_DATE)
            if bday:
                birthdate = bday

    if not admission_datetime:
        for word in ADMISSIONDATE_LIST:
            admission = extract_info(line, word, REGEX_TIME)
            if admission:
                admission_datetime = format_datetime(admission)

    if not analysis_datetime:
        for word in ANALYZEDATE_LIST:
            analysis = extract_info(line, word, REGEX_TIME)
            if analysis:
                analysis_datetime = format_datetime(analysis)

    return {"casenumber": casenumber,
            "birthdate": birthdate,
            "admission_datetime": admission_datetime,
            "analysis_datetime": analysis_datetime}


def extract_info(line: str, variable: str, pattern: str) -> List[Any]:
    """
    Extract specific information from line using the following re:
    r{variable}{dot}*: {2,5}{pattern}.

    Parameters
    ----------
    line : str
        text lines
    variable : str
        Regex
    pattern : str
        Regular expression to match the variable value

    Returns
    -------
    list
        list to append the result of info
    """
    output = None
    idx = line.find(variable)

    if idx >= 0:
        line = line[idx:]

        line_search = re.search(f"{variable}\\.*: {{0,5}}{pattern}", line)

        if line_search:
            res = line_search.group(0)
            res = re.sub(f"{variable}\\.*: {{0,2}}", "", res)
            res = res.strip()

            output = res.strip()

    return output
